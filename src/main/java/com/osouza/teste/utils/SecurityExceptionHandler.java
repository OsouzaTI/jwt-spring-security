package com.osouza.teste.utils;

import com.osouza.teste.annotations.ExceptionHandlerMessage;
import com.osouza.teste.domain.model.ExceptionHandlerResponse;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.HandlerMethod;

import java.lang.reflect.Method;

@ControllerAdvice
public class SecurityExceptionHandler {

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ExceptionHandlerResponse> handlerException(RuntimeException exception, HandlerMethod handler) {
        ExceptionHandlerMessage message = AnnotationUtils.findAnnotation(handler.getMethod(), ExceptionHandlerMessage.class);
        ExceptionHandlerResponse exceptionHandlerResponse = new ExceptionHandlerResponse();
        exceptionHandlerResponse.setException(exception.getMessage());

        if (message != null) {
             exceptionHandlerResponse.setMessage(message.message());
        } else {
            exceptionHandlerResponse.setMessage(ExceptionHandlerResponse.DEFAULT_MESSAGE);
        }

        return ResponseEntity.badRequest().body(exceptionHandlerResponse);

    }

}
