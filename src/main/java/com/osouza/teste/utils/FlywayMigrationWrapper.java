package com.osouza.teste.utils;

import jakarta.annotation.PostConstruct;
import jakarta.persistence.PostLoad;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@Component
public class FlywayMigrationWrapper {
    
    private final Flyway flyway;

    public FlywayMigrationWrapper(
        @Value("${spring.datasource.url}")      String url,
        @Value("${spring.datasource.username}") String username,
        @Value("${spring.datasource.password}") String password
    ) {
        this.flyway = Flyway.configure()
            .cleanDisabled(false)
            .dataSource(buildDatasource(url, username, password)).load();
    }

    private DataSource buildDatasource(String url, String username, String password) {
        return DataSourceBuilder.create().url(url).username(username).password(password).build();
    }

    public void migrate() {
        flyway.clean();
        flyway.migrate();
    }

}
