package com.osouza.teste.repository;

import org.springframework.stereotype.Repository;

import com.osouza.teste.domain.entity.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface ClienteRepository extends JpaRepository<Cliente, Long> {}
