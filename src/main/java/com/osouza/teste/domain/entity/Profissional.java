package com.osouza.teste.domain.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.PrimaryKeyJoinColumn;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Profissional extends Usuario {

    @Id
    private Long id;

    @Column(unique = true)
    private String nome;

    @ManyToOne
    @NotNull
    private Conselho conselho;

    @NotNull
    private Integer conselhoEstado;

    @NotNull
    private String numeroRegistro;

    @ManyToOne
    @NotNull
    private Especialidade especialidade;

    private Boolean ativo = false;

}
