package com.osouza.teste.domain.entity;

import java.time.LocalDate;
import java.util.Date;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class Cliente extends Usuario {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotBlank
    private String nome;

    private String nomeSocial;

    @NotBlank
    @Size(min = 14, max = 14)
    private String cpf;

    @NotNull
    private LocalDate dataAniversario;

    @NotNull
    private Integer genero;

    @NotNull
    private Integer corPeleOuRaca;

    @Size(max = 255)
    private String profissao;

    private Integer escolaridade;

    private Integer estadoCivil;

    @Size(max = 255)
    private String naturalidade;

    @Size(max = 255)
    private String nomeMae;

    @Size(max = 255)
    private String nomePai;

    @Size(max = 15)
    private String telefone;

    private Boolean ativo = true;

    @Temporal(TemporalType.TIMESTAMP)
    private Date createdAt;

    @Temporal(TemporalType.TIMESTAMP)
    private Date updatedAt;

    @ManyToOne
    private ClienteTipo tipo;
}
